#!/bin/bash

sudo add-apt-repository -y ppa:jonathonf/python-3.6
sudo apt-get update
sudo apt-get install -y build-essential libssl-dev libffi-dev python3.6 python3.6-dev python3.6-venv
wget https://bootstrap.pypa.io/get-pip.py
sudo python3.6 get-pip.py
sudo ln -s /usr/bin/python3.6 /usr/local/bin/python3
sudo ln -s /usr/bin/python3.6 /usr/local/bin/python
sudo pip3 install -U pip setuptools
sudo pip3 install ansible==2.3.0
sudo cp /usr/local/bin/ansible /usr/bin/ansible
sudo ln -s /usr/bin/python3 /usr/bin/python

mkdir -p ~/ansible/
cat > ~/ansible/.ansible.cfg <<EOF
[defaults]
remote_tmp = /vagrant/ansible/tmp
log_path = /vagrant/ansible/ansible.log
EOF