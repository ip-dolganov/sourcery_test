from urllib.parse import urlencode


class TrafficReceiverError(Exception):
    """
    Ошибка парсинга каналов (ошибка сервера нетворка и т.д.)
    """


class ClientUnknownError(Exception):
    """
    Неопознанная ошибка клиента: не те параметры переданы, не хватает параметров
    и т.д.
    """

class ClientKnownError(Exception):
    """
    Известная ошибка клиента: упал сервер, слишком много реквестов и т.д.
    """


class Parser(object):
    """
    Родительский класс для всех парсеров.
    """
    required_auth_fields = None

    def __init__(self, auth):
        """
        При инициализации парсера передаем ему redis. Проверяем,
        что все нужные авторизационные поля добавлены
        (и что в классе вообще есть проверка на наличие этих полей)
        """
        if not self.required_auth_fields:
            raise ClientKnownError(
                f'required_auth_fields are not set in {self.__class__.__name__}'
            )
        for field in self.required_auth_fields:
            if field not in auth:
                raise ClientKnownError(
                    f'field {field} is missing '
                    f'in {self.__class__.__name__} auth'
                )
            if not auth[field]:
                raise ClientKnownError(
                    f'field {field} is empty '
                    f'in {self.__class__.__name__} auth'
                )
        self.auth = auth

    def _raise_error(self, text, trace=True):
        """
        Вызвать неизвестную ошибку, предварительно
        прицепив название класса, ее вызвавшего.
        :param trace: если True - вызывать Unknown ошибку, к которой обработчик
        прицепит трейс.
        Иначе вызвать Known ошибку, к которой трейс прицеплен не будет.
        """
        text = f'{self.__class__.__name__} unknown error: {text}'
        if trace:
            raise ClientUnknownError(text)
        else:
            raise ClientKnownError(text)
